from . import app

@app.route('/')
def index():
    return 'Hello World!'

@app.route('/randomize/<string>', methods=['GET'])
def randomize(string):
    data = string
    swapData = ''.join((data[:1], data[3], data[:1], data[3], data[5], data[7], data[5], data[7]))
    return swapData