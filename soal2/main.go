package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"log"
	"os"
	"io/ioutil"
)

type Response struct {
	Status bool `json:"status"`
}

type DataStruct struct {
    Data string
}

func Register(w http.ResponseWriter, req *http.Request) {

	// Read body
	b, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// Unmarshal
	var msg DataStruct
	err = json.Unmarshal(b, &msg)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonData, err := json.Marshal(msg)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Println(string(jsonData))

	// Get Params
	names, ok := req.URL.Query()["name"]
	if !ok || len(names) < 1 {
		resp := Response{Status: true}
		fmt.Println(resp)
		byte, err := json.Marshal(resp)
		if err != nil {
			return
		}

		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, string(byte))
	
		fmt.Println(string(byte))
		log.Println("Url Param 'name' is missing")
		return
	}
	name := names[0]

	contentType, ok := req.URL.Query()["type"]
	if !ok || len(contentType) < 1 {
		resp := Response{Status: false}
		fmt.Println(resp)
		byte, err := json.Marshal(resp)
		if err != nil {
			return
		}

		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, string(byte))
	
		fmt.Println(string(byte))

		log.Println("Url Param 'type' is missing")
		return
	}
	types := contentType[0]

	jsonFile, err := os.Create("./items/" + string(name) + "." + string(types))
        if err != nil {
                panic(err)
        }
        defer jsonFile.Close()
	jsonFile.Write(jsonData)
	jsonFile.Close()
}

func Deregister(w http.ResponseWriter, req *http.Request) {

}

func Retrieve(w http.ResponseWriter, req *http.Request) {

}

func main() {
	http.HandleFunc("/register", Register)
 	http.HandleFunc("/deregister", Deregister)
 	http.HandleFunc("/retrieve", Retrieve)
	address := ":8090"
	log.Println("Starting server on ", address)
	err := http.ListenAndServe(address, nil)
	if err != nil {
		panic(err)
	}
}
