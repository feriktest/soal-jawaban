Soal 1 -> Python - Flask
========================

# Usage
Open console and run:
```
python run.py
```

Register
------------------------
Options:
1. using Postman
```
POST : http://127.0.0.1:5000/register/contoh1/json
or
POST : http://127.0.0.1:5000/register/contoh1/xml

Json Body : {"status": "OK", "message": "hello register"}
```

2. using console
```
curl -X POST \
  http://127.0.0.1:5000/register/contoh1/json \
  -H 'content-type: application/json' \
  -d '{"status": "OK", "message": "hello register"}'
```

Result:
```
new file will automatically added in "items"
```


Deregister
------------------------
Options:
1. using Postman
```
POST : http://127.0.0.1:5000/deregister/contoh1.json
```

2. using console
```
curl -X POST \
  http://127.0.0.1:5000/deregister/contoh1.json
```

Result:
```
file contoh1.json in "items" will be removed
```


Retrieve
------------------------
Options:
1. using Postman
```
GET : http://127.0.0.1:5000/retrieve/contoh1.json
```

2. using console
```
curl -X GET \
  http://127.0.0.1:5000/retrieve/contoh1.json
```

Result:
```
will return content of contoh1.json
```