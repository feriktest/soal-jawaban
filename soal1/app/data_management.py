import json
import os
from dicttoxml import dicttoxml

class DataManagement():
    def register(self, name, itemContent, type):
        try:
            with open('items/' + name + '.' + type, 'w') as f:
                if type and type == 'json':
                    json.dump(itemContent, f)
                elif type and type == 'xml':
                    xml = dicttoxml(itemContent, custom_root='data-output', attr_type=False)
                    f.write(xml)
                return json.dumps({"status": True})
        except Exception:
            return json.dumps({"status": False})


    def deregister(self, name):
        try:
            os.remove(os.path.join('items/', name))
            return json.dumps({"status": True})
        except Exception:
            return json.dumps({"status": False})


    def retrieve(self, itemName):
        try:
            file = open('items/' + itemName, 'r')
            return file.read()
        except Exception:
            return json.dumps({"status": False})