from . import app
from .data_management import DataManagement
import json
from flask import request

@app.route('/')
def index():
    return 'Hello World!'

@app.route('/register/<name>/<type>', methods=['POST'])
def register(name, type):
    data = request.data
    content = json.loads(data)
    register_data = DataManagement().register(name, content, type)
    return register_data

@app.route('/deregister/<name>', methods=['POST'])
def deregister(name):
    deregister_data = DataManagement().deregister(name)
    return deregister_data

@app.route('/retrieve/<name>', methods=['GET'])
def retrieve(name):
    retrieve_data = DataManagement().retrieve(name)
    return retrieve_data